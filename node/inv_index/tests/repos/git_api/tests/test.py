from ..git import *
import pytest

def test_with_login():
    git = Git("Andrewerr", "Qazwsx12")
    assert git.search_repository("a") != None

def test_no_login():
    git = Git()
    assert git.search_repository("a") != None

def test_zero_result():
    git = Git()
    assert git.search_repository("afklvmfkjgkljbokjritjhgkjrthgkjrthghkhjkrngkrnkjhgkjhgkjhrtkjghkjtrhg") == []
